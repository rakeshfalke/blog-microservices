const express = require('express');
const { randomBytes } = require('crypto');
const bodyParser = require('body-parser');
const cors = require('cors');
const axios = require('axios');

const app = express();
app.use(bodyParser.json());

app.post('/events', (req, res) => {
    const event = req.body;
    axios.post('http://posts-clusterip-srv:4000/events', event)
    .catch((err) => {
        console.log(err.message);
        console.log("POST event from event bus");
    });
    axios.post('http://comments-srv:4001/events', event).catch((err) => {
        console.log(err.message);
    });
    axios.post('http://query-srv:4002/events', event).catch((err) => {
        console.log(err.message);
    });
    axios.post('http://moderation-srv:4003/events', event).catch((err) => {
        console.log(err.message);
    });
    res.send({ status: 'OK' });
});

app.listen(4005, () => {
    console.log('Event Bus Service Listening on 4005');
});