const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const app = express();
app.use(bodyParser.json());


app.post('/events',  async (req, res) => {
    const { type, data } = req.body;
    if (type == 'CommentCreated') {
        const { id, content, postId, status } = data;
        const updatedstatus = content.includes('red') ? 'rejected' : 'approved';
        await axios.post('http://event-bus-srv:4005/events', {
            type: 'CommentModerated',
            data: {
                id: id, 
                content: content, 
                postId: postId,
                status: updatedstatus
            }
        }).catch((err) => {
            console.log(err.message);
        });
    }
    res.send({});
});

app.listen(4003, () => {
    console.log('Moderation Service Listening on 4003');
});