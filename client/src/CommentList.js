import React from "react";

const CommentList = ({ comments }) => {
  
  const renderedComments = comments.map((comment) => {
    let commentRender = comment.status;
    if(comment.status === 'approved') {
      commentRender = comment.content;
    }
    return <li key={comment.id}>{commentRender}</li>;
  });

  return <ul>{renderedComments}</ul>;
};

export default CommentList;
