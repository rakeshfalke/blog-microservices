# Blog using multiple microservices

Blog with 5 microservices and docker k8s

## Getting docker images

- [ ] [React Client App](https://hub.docker.com/r/rakeshfalke/client)
- [ ] [Post Service](https://hub.docker.com/r/rakeshfalke/posts)
- [ ] [Event Bus Service](https://hub.docker.com/r/rakeshfalke/event-bus)
- [ ] [Query Service](https://hub.docker.com/r/rakeshfalke/query-service)
- [ ] [Comment Service](https://hub.docker.com/r/rakeshfalke/comments)
- [ ] [Moderatio Service](https://hub.docker.com/r/rakeshfalke/moderation)

## Ingress nginx server

- [ ] [Ingress Nginx](https://kubernetes.github.io/ingress-nginx/deploy/#docker-desktop)
